#ifndef merge1
#define merge1

#include <vector>
#include <cmath>
#include <iostream>



struct alpha {
  bool operator() (const string& left, const string& right) {
    return left < right;
  }
};

struct num {
  bool operator() (const string& left, const string& right) {
    return left < right;
  }
};

struct num1 {
  bool operator() (const string& left, const string& right) {
    return left > right;
  }
};

struct alpha1 {
  bool operator() (const string& left, const string& right) {
    return left > right;
  }
};


template <class T, class Comparator>
void mergeSort (std::vector<T>& myArray, Comparator comp) {
  int first;
  first = 0;
  mergeSort_helper(myArray, comp, first, myArray.size()-1);
};

template <class T, class Comparator>
void mergeSort_helper(std::vector<T>& myArray, Comparator comp, int l, int r) {
  if(l<r) {
    int m = (l+r)/2;
    mergeSort_helper(myArray, comp, l, m);
    mergeSort_helper(myArray, comp, m+1, r);
    merge_helper(myArray, comp, l, r, m);
  }
};

template <class T, class Comparator>
void merge_helper(std::vector<T>& myArray, Comparator comp, int l, int r, int m) {
  int i = l, j = m+1, k = 0;
  std::vector<T> temp;
  while (i <= m || j <= r) {
    if (i <= m && (j > r || comp(myArray[i], myArray[j]))) {
      temp.push_back(myArray[i]);
      i++;
      k++;
    } else {
      temp.push_back(myArray[j]);
      j++;
      k++;
    }
  }
  for (int it=0; it < r+1 - l; it++) {
    myArray[it+l] = temp[it];
  }
}

#endif