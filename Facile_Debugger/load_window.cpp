#include "load_window.h"
using namespace std;

LoadWindow::LoadWindow()
{
	// Title
	setWindowTitle("Load Window");

	// Overall layout
	overallLayout = new QVBoxLayout();

	buttonsLayout = new QHBoxLayout();
	formLayout = new QHBoxLayout();
	overallLayout->addLayout(formLayout);
	overallLayout->addLayout(buttonsLayout);

	FilenameLabel = new QLabel("Facile Program Filename:");
	formLayout->addWidget(FilenameLabel);

	// Image filename input
	//TODO
	FilenameInput = new QLineEdit();
	formLayout->addWidget(FilenameInput);

	// Add button
	//TODO
	LoadFileButton = new QPushButton("Load File");
	connect(LoadFileButton, SIGNAL(clicked()), this, SLOT(loadFile()));
	buttonsLayout->addWidget(LoadFileButton);

	connect(FilenameInput, SIGNAL(returnPressed()), this, SLOT(loadFile()));
	buttonsLayout->addWidget(LoadFileButton);


	quitButton = new QPushButton("Quit");
	//add signal and slot
	connect(quitButton, SIGNAL(clicked()), this, SLOT(quit()));
	buttonsLayout->addWidget(quitButton);
	error = new ErrorWindow();
	string nothing;
	nothing = "no errors yet";
	error->set_text(nothing);
	error->hide();

	// Set overall layout
	setLayout(overallLayout);
}

LoadWindow::~LoadWindow(){}



void LoadWindow::loadFile(){
	string filename;

		if(FilenameInput->text().isEmpty())
		{
			return;
		}
		else
		{
			filename = FilenameInput->text().toStdString();
			ifstream infile(filename.c_str());
			if (!infile)
	        {
	        	string error_text;
	        	error_text = "ERROR: Invalid File Name";
	        	error->clearText();
	        	error->set_text(error_text);
			   	//error->show();
			   	FilenameInput->clear();
			   	loadFile();
			   	return;
	        }
        interpretProgram(infile, cout);
        hide();

	}
}

void LoadWindow::quit(){
	QApplication::exit();
}


void LoadWindow::parseProgram(istream &inf, vector<Statement *> & program)
{
	program.push_back(NULL);
	
	string line;
	while( ! inf.eof() )
	{
		getline(inf, line);
		program.push_back( parseLine( line ) );
		lines.push_back(line);
	}
}


Statement * LoadWindow::parseLine(string line)
{
	Statement * statement;	


	stringstream ss;
	string type;
	string var;
	string var2;
	string op;
	int val, comp, operator_;

	ss << line;//putting the current line into a stringstream
	ss >> type;//pulling the statement type out of stringstream
	
	if ( type == "LET" )//lets statement
	{
		ss >> var;
		ss >> val;
		// Note:  Because the project spec states that we can assume the file
		//	  contains a syntactically legal Facile program, we know that
		//	  any line that begins with "LET" will be followed by a space
		//	  and then a variable and then an integer value.
		statement = new LetStatement(var, val);
	}

	if (type == "PRINT"){//print statement
		ss >> var;
		statement = new PrintStatement(var);
	}

	if ( (type == "END")|| (type == ".") ){//end statement or . statement
		statement = new EndStatement();
	}

	if (type == "ADD"){//add statement
		ss >> var;
		while((ss.peek() == 32) || (ss.peek() == 9)){//checking for whitespace
			ss.get();//getting rid of whitespace
		}
		if((ss.peek()<=57) && (ss.peek() >= 48)){//checking if adding a number or variable
			ss >> val;
			statement = new AddStatement(var, val);
		}
		else{
			ss >> var2;
			statement = new AddStatement(var, var2);
		}
	}

	if (type == "SUB"){//subtraction statement
		ss >> var;
		while((ss.peek() == 32) || (ss.peek() == 9)){//checking for whitespace
			ss.get();//getting rid of whitespace
		}
		if((ss.peek()<=57) && (ss.peek() >= 48)){//checking if subrating a number or variable
			ss >> val;
			statement = new SubStatement(var, val);
		}
		else{
			ss >> var2;
			statement = new SubStatement(var, var2);
		}
	}

	if (type == "MULT"){//multiply statement
		ss >> var;
		while((ss.peek() == 32) || (ss.peek() == 9)){
			ss.get();//getting rid of whitespace
		}
		if((ss.peek()<=57) && (ss.peek() >= 48)){//checking if multiplying by a number or variable
			ss >> val;
			statement = new MultStatement(var, val);
		}
		else{
			ss >> var2;
			statement = new MultStatement(var, var2);
		}
	}


	if (type == "DIV"){//dividing statement
		ss >> var;
		while((ss.peek() == 32) || (ss.peek() == 9)){//checking if dividing by a number or variable
			ss.get();//getting rid of whitespace
		}
		if((ss.peek()<=57) && (ss.peek() >= 48)){
			ss >> val;
			statement = new DivStatement(var, val);
		}
		else{
			ss >> var2;
			statement = new DivStatement(var, var2);
		}
	}

	if (type == "GOTO"){//goto statement
		ss >> val;
		statement = new GotoStatement(val);
	}

	if (type == "IF"){
	
		ss >> var;
		ss >> op;
		ss >> comp;
		string trash;
//		ss >> trash;
		ss >> val;

			//checking what type of opperator is used
			//and assigning a number based off the 
			//type of opperator
			if(op == ">"){
				operator_ = -1;
			}

			if(op == ">="){
				operator_ = -11;
			}
			if(op == "<"){
				operator_ = -2;
			}
			if(op == "<="){
				operator_ = -22;
			}
			if(op == "="){
				operator_ = -3;
			}
			if(op == "<>"){
				operator_ = -33;
			}

		statement = new IfStatement(var, operator_, comp, val);
	}

	if (type == "GOSUB"){//Go sub statement
		ss >> val;
		statement = new GoSubStatement(val);
	}

	if (type == "RETURN"){//return statement
		statement = new ReturnStatement();
	}
	if (type == "PRINTALL"){//print all statement
		statement = new PrintAllStatement();
	}	
		
	return statement;//returning whatever statement the line contained
}


void LoadWindow::interpretProgram(istream& inf, ostream& outf)
{
	vector<Statement *> program;
	parseProgram( inf, program);
	int size = program.size();

	ProgramState* currState = new ProgramState(size, error);
	debugger = new debugger_window(lines, currState, program);
	debugger->show();
}
