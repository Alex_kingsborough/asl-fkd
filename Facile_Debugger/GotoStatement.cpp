#include "GotoStatement.h"
#include <iostream>
#include <stdlib.h>
using namespace std;
GotoStatement::GotoStatement(int newline)
{new_line = newline;}



void GotoStatement::execute(ProgramState * state, ostream &outf)
{
	// TODO
	int max = state->return_m_numLines();
	if((new_line < 1) || (new_line > max)){
			state->clearError();
			string error;
			error = "ERROR: Illegal Jump Instruction";
			state->showError(error);
			state->ended();		
	}
	else{
		state->gotoline(new_line);
	}
}