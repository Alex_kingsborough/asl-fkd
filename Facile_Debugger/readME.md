This is an interpreter for basic Facile programs.  It supports addition,
subtraction, multiplication, division, if statements, and gosub/return
statement functions.

The program also has a graphical debugger which points out errors, allows
the user to step through the code, set breakpoints and see the values of
the current variables.
