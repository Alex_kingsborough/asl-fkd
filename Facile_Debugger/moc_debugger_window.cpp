/****************************************************************************
** Meta object code from reading C++ file 'debugger_window.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "debugger_window.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'debugger_window.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_debugger_window[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x08,
      29,   16,   16,   16, 0x08,
      39,   16,   16,   16, 0x08,
      46,   16,   16,   16, 0x08,
      53,   16,   16,   16, 0x08,
      60,   16,   16,   16, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_debugger_window[] = {
    "debugger_window\0\0_continue()\0inspect()\0"
    "step()\0next()\0quit()\0_break()\0"
};

void debugger_window::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        debugger_window *_t = static_cast<debugger_window *>(_o);
        switch (_id) {
        case 0: _t->_continue(); break;
        case 1: _t->inspect(); break;
        case 2: _t->step(); break;
        case 3: _t->next(); break;
        case 4: _t->quit(); break;
        case 5: _t->_break(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData debugger_window::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject debugger_window::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_debugger_window,
      qt_meta_data_debugger_window, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &debugger_window::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *debugger_window::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *debugger_window::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_debugger_window))
        return static_cast<void*>(const_cast< debugger_window*>(this));
    return QWidget::qt_metacast(_clname);
}

int debugger_window::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
