#include "values_window.h"
#include "merge.h"


values_window::values_window(ProgramState * state)
{
	// Title
	setWindowTitle("Values Window");


	_state = state;
	v = _state->map_to_num_vector();

	// Overall layout
	overallLayout = new QHBoxLayout();

	valuesListWidget = new QListWidget();
	overallLayout->addWidget(valuesListWidget);

	buttonsLayout = new QVBoxLayout();

	updateButton = new QPushButton("Update");
	//add signal and slot
	connect(updateButton, SIGNAL(clicked()), this, SLOT(update()));
	buttonsLayout->addWidget(updateButton);

	hideButton = new QPushButton("Hide");
	//add signal and slot
	connect(hideButton, SIGNAL(clicked()), this, SLOT(hide()));
	buttonsLayout->addWidget(hideButton);

	firstRadioButton = new QRadioButton("increasing value");
	buttonsLayout->addWidget(firstRadioButton);

	secondRadioButton = new QRadioButton("decreasing values");
	buttonsLayout->addWidget(secondRadioButton);

	thirdRadioButton = new QRadioButton("increasing alpha");
	buttonsLayout->addWidget(thirdRadioButton);

	forthRadioButton = new QRadioButton("decreasing alpha");
	buttonsLayout->addWidget(forthRadioButton);


	overallLayout->addLayout(buttonsLayout);

	num alph;
	mergeSort(v, alph);

	int size = v.size();
	for(int i = 0; i < size; i++){
		QString qstr = QString::fromStdString(v[i]);
		valuesListWidget->addItem(qstr);
	}

	// Set overall layout
	setLayout(overallLayout);
}

values_window::~values_window(){}

void values_window::hide(){
	close();
}

void values_window::update(){
	if(firstRadioButton->isChecked()){
		v.clear();
		v = _state->map_to_num_vector();
		valuesListWidget->clear();
		num alph;
		mergeSort(v, alph);

		int size = v.size();
		for(int i = 0; i < size; i++){
			QString qstr = QString::fromStdString(v[i]);
			valuesListWidget->addItem(qstr);
		}
	}

	else if(secondRadioButton->isChecked()){
		v.clear();
		v = _state->map_to_num_vector();
		valuesListWidget->clear();
		num1 alph;
		mergeSort(v, alph);

		int size = v.size();
		for(int i = 0; i < size; i++){
			QString qstr = QString::fromStdString(v[i]);
			valuesListWidget->addItem(qstr);
		}
	}	
	
	else if(thirdRadioButton->isChecked()){
		v.clear();
		v = _state->map_to_alpha_vector();
		valuesListWidget->clear();
		alpha alph;
		mergeSort(v, alph);

		int size = v.size();
		for(int i = 0; i < size; i++){
			QString qstr = QString::fromStdString(v[i]);
			valuesListWidget->addItem(qstr);
		}
	}

	else if(forthRadioButton->isChecked()){
		v.clear();		
		v = _state->map_to_alpha_vector();
		valuesListWidget->clear();
		alpha1 alph;
		mergeSort(v, alph);

		int size = v.size();
		for(int i = 0; i < size; i++){
			QString qstr = QString::fromStdString(v[i]);
			valuesListWidget->addItem(qstr);
		}
	}
	else{
		v.clear();
		v = _state->map_to_alpha_vector();
		valuesListWidget->clear();
		alpha alph;
		mergeSort(v, alph);

		int size = v.size();
		for(int i = 0; i < size; i++){
			QString qstr = QString::fromStdString(v[i]);
			valuesListWidget->addItem(qstr);
		}
	}
}




