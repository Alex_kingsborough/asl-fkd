#ifndef ADD_STATEMENT_INCLUDED
#define ADD_STATEMENT_INCLUDED

#include "Statement.h"
#include "ProgramState.h"

class AddStatement: public Statement
{
private:
	int first_value;
	int second_value;
	std::string variable_Name2;
	std::string m_variableName;


public:
	AddStatement(std::string, int);	
	AddStatement(std::string, std::string);
	virtual void execute(ProgramState * state, std::ostream &outf);

	
};

#endif