#ifndef SUB_STATEMENT_INCLUDED
#define SUB_STATEMENT_INCLUDED

#include "Statement.h"
#include "ProgramState.h"

class SubStatement: public Statement
{
private:
	int first_value;
	int second_value;
	std::string variable_Name2;
	std::string m_variableName;


public:
	SubStatement(std::string, int);
	SubStatement(std::string, std::string);	
	virtual void execute(ProgramState * state, std::ostream &outf);

	
};

#endif