#include "GoSubStatement.h"
#include <iostream>
#include <stdlib.h>
using namespace std;
GoSubStatement::GoSubStatement(int newline)
{
	new_line = newline;
}



void GoSubStatement::execute(ProgramState * state, ostream &outf)
{
	// TODO
	int max = state->return_m_numLines();
		cout << "max" << max << endl;

	if((new_line < 1) || (new_line > max)){
			state->clearError();
			string error;
			error = "ERROR: Illegal Jump Instruction";
			state->showError(error);
			state->ended();		
	}
	else{
		int pushed;
		pushed = state->getCurrLine();
		state->push(pushed);
		state->gotoline(new_line);
	}
}