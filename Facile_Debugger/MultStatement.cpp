#include "MultStatement.h"
#include <iostream>
using namespace std;

MultStatement::MultStatement(std::string variableName, int value2)
{
	second_value = value2;
	m_variableName = variableName;
}

MultStatement::MultStatement(std::string variableName1, std::string variableName2){
	second_value = -123456789;
	m_variableName = variableName1;
	variable_Name2 = variableName2;
}


// The LetStatement version of execute() should make two changes to the
// state of the program:
//
//    * set the value of the appropriate variable
//    * increment the program counter
void MultStatement::execute(ProgramState * state, ostream &outf)
{
	// TODO
	if (second_value == -123456789){
	second_value = state->return_int(variable_Name2);
	}

	first_value = state->return_int(m_variableName);
	first_value = state->mult(first_value, second_value);

	state->increment();
	state->newMapPair(m_variableName, first_value);

}
