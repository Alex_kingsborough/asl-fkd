// ProgramState.h
//
// CS 104 / Spring 2016
//
// The ProgramState class encapsulates the state of an executing Facile
// program.  The state of a Facile program consists of three parts:
//
//    * The program counter, which specifies the line number of the
//      statement that will execute next.
//    * A map, as explained by the problem writeup.
//    * A stack of integers, which is used to hold return lines for GOSUB
//      statements.
//
// I've provided you a start on this class, but you'll need to add methods
// to it as you go along.  In particular, you'll need methods to access and
// modify the state; these methods will be called primarily by the various
// execute() methods in the Statement subclasses, to allow the execution of
// a statement to change the state of a program.  For example, executing a
// GOTO statement will cause the program counter to be changed.

#ifndef PROGRAM_STATE_INCLUDED
#define PROGRAM_STATE_INCLUDED
#include <map>
#include <string>
using namespace std; 
#include "stackint.h"
#include "error_window.h"



class ProgramState
{
public:
	ProgramState(int numLines, ErrorWindow*);
	~ProgramState();

	// You'll need to add a variety of methods here.  Rather than trying to
	// think of what you'll need to add ahead of time, add them as you find
	// that you need them.

	void showError(std::string);
	void clearError();
//let statement
	void increment();

	int getCurrLine();

	void newMapPair(std::string, int);

//end statement
	void ended();

//add statement
	int add(int, int);
	int return_int(std::string);
//sub statement
	int sub(int, int);
	int mult(int, int);
	int div(int, int);
//goto statement
	void gotoline(int);
	int return_m_numLines();
//stack functions
	void push(int);
	void pop();
	int top();
	bool empty();
//setting current line functoin
	void set_curr_line(int);
//print all function
	void print_all();
//switch running
	void on_running();
	void off_running();
	bool is_running();
	void add_step();
	int return_steps();
	void sub_step();
	std::vector<string> map_to_alpha_vector();
	std::vector<string> map_to_num_vector();


private:
	int m_numLines;
	int currLine;
	int running;
	int steps;
	StackInt stack;
	//variables
	std::map<std::string, int> variables;
	ErrorWindow * _error;
	std::vector<string> string_vect;

};

#endif



