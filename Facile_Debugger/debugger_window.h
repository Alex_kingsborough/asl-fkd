#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QListWidget>
#include <QString>
#include <string>
#include <vector>
#include <fstream>
#include <QApplication>
#include <QCloseEvent>
#include <QListWidget>
#include <iostream>

#include <QtGui>

#include <sstream> 
#include <cstdlib>

//#include "ProgramState.h"
#include "Statement.h"
#include "values_window.h"


class debugger_window : public QWidget
{
	Q_OBJECT
public:
	debugger_window(std::vector<std::string>, ProgramState *, vector<Statement *>);
	~debugger_window();

private slots:
	void _continue();
	void inspect();
	void step();
	void next();
	void quit();
	void _break();

private:

	QHBoxLayout* overallLayout;
	QVBoxLayout* buttonsLayout;
	QPushButton* breakButton;
	QPushButton* continueButton;
	QPushButton* stepButton;
	QPushButton* nextButton;
	QPushButton* inspectButton;
	QPushButton* quitButton;
	QListWidget* lineListWidget;
	values_window* values;
	std::vector<std::string> text;
	ProgramState * _state;
	vector<Statement *> _program;
	int break_array[1000];

	//ostream _outf;

};