#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QListWidget>
#include <string>
#include <vector>
#include <QApplication>
#include <QRadioButton>
#include "ProgramState.h"
//#include "merge.h"
//#include "merge.h"



class values_window : public QWidget
{
	Q_OBJECT
public:
	values_window(ProgramState * state);
	~values_window();

private slots:
	void hide();
	void update();

private:

	QHBoxLayout* overallLayout;
	QVBoxLayout* buttonsLayout;
	QPushButton* hideButton;
	QPushButton* updateButton;
	QListWidget* valuesListWidget;
	QRadioButton* firstRadioButton;
	QRadioButton* secondRadioButton;
	QRadioButton* thirdRadioButton;
	QRadioButton* forthRadioButton;
	ProgramState * _state;
	std::vector<string> v;



};