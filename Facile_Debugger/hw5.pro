######################################################################
# Automatically generated by qmake (2.01a) Wed Mar 15 16:28:03 2017
######################################################################

TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .

# Input
HEADERS += AddStatement.h \
           debugger_window.h \
           DivStatement.h \
           EndStatement.h \
           error_window.h \
           GoSubStatement.h \
           GotoStatement.h \
           IfStatement.h \
           LetStatement.h \
           llistint.h \
           load_window.h \
           merge.h \
           MultStatement.h \
           PrintAllStatement.h \
           PrintStatement.h \
           ProgramState.h \
           ReturnStatement.h \
           stackint.h \
           Statement.h \
           SubStatement.h \
           values_window.h
SOURCES += AddStatement.cpp \
           debugger_window.cpp \
           DivStatement.cpp \
           EndStatement.cpp \
           error_window.cpp \
           GoSubStatement.cpp \
           GotoStatement.cpp \
           IfStatement.cpp \
           LetStatement.cpp \
           llistint.cpp \
           load_window.cpp \
           main.cpp \
           MultStatement.cpp \
           PrintAllStatement.cpp \
           PrintStatement.cpp \
           ProgramState.cpp \
           ReturnStatement.cpp \
           stackint.cpp \
           SubStatement.cpp \
           values_window.cpp
