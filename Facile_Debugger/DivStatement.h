#ifndef DIV_STATEMENT_INCLUDED
#define DIV_STATEMENT_INCLUDED

#include "Statement.h"
#include "ProgramState.h"

class DivStatement: public Statement
{
private:
	int first_value;
	int second_value;
	std::string variable_Name2;
	std::string m_variableName;


public:
	DivStatement(std::string, int);	
	DivStatement(std::string, std::string);
	virtual void execute(ProgramState * state, std::ostream &outf);

	
};

#endif