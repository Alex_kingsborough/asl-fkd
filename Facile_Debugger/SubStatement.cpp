#include "SubStatement.h"
#include <iostream>
using namespace std;

SubStatement::SubStatement(std::string variableName, int value2)
{
	second_value = value2;
	m_variableName = variableName;
}

SubStatement::SubStatement(std::string variableName1, std::string variableName2){
	second_value = -123456789;
	m_variableName = variableName1;
	variable_Name2 = variableName2;

}


// The LetStatement version of execute() should make two changes to the
// state of the program:
//
//    * set the value of the appropriate variable
//    * increment the program counter
void SubStatement::execute(ProgramState * state, ostream &outf)
{
	// TODO
	if (second_value == -123456789){
	second_value = state->return_int(variable_Name2);
	}

	first_value = state->return_int(m_variableName);
	first_value = state->sub(first_value, second_value);

	state->increment();
	state->newMapPair(m_variableName, first_value);

}
