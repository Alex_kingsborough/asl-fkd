#include "ProgramState.h"
#include <iostream>

using namespace std;

//constructor
ProgramState::ProgramState(int numlines, ErrorWindow * error){
	_error = error;
	m_numLines = numlines;
	currLine = 1;
	running = 0;
	steps = 0;
}
void ProgramState::showError(std::string str){
	_error->set_text(str);
}

void ProgramState::clearError(){
	_error->clearText();
}



//increase current line by 1
void ProgramState::increment(){
	//if(running = 1){
		currLine = currLine + 1;
	//}
}

void ProgramState::add_step(){
	steps++;
}

int ProgramState::return_steps(){return steps;}
void ProgramState::sub_step(){steps--;}

//switch running from 1 to 0 or 0 to 1
void ProgramState::on_running(){running = 1;}
void ProgramState::off_running(){running = 0;}
//check if program is running or not
bool ProgramState::is_running(){
	if(running==1){return true;}
	else{return false;}
}

//make a new map pair for a variable
void ProgramState::newMapPair(std::string var, int val){
	variables[var] = val;
}
//return the current line
int ProgramState::getCurrLine(){
	return currLine;
}

void ProgramState::ended(){
	currLine = 0;  //set the line to an unattainable number
	//then if in main we see that number we know to exit
	variables.clear();
}
//add two values together and return the answer
int ProgramState::add(int value1, int value2){
	value1 = value1 + value2;
	return value1;
}
//subtract value 1 from value 2 and return the result
int ProgramState::sub(int value1, int value2){
	value1 = value1 - value2;
	return value1;
}
//multiply two values and return the answer
int ProgramState::mult(int value1, int value2){
	value1 = value1 * value2;
	return value1;
}
//divid the first value by the second value and
//return the answer
int ProgramState::div(int value1, int value2){
	value1 = value1 / value2;
	return value1;
}
//return the value of a variable
int ProgramState::return_int(std::string var){
	return variables[var];
}
//set current line to a new line
void ProgramState::gotoline(int line){
	currLine = line;
}
//return the number of lines a program has
int ProgramState::return_m_numLines(){
	return m_numLines;
}
// stack
//push a value to the stack
void ProgramState::push(int push){
	stack.push(push);
}

//pop a value from the stack
void ProgramState::pop(){
	stack.pop();
}

//look at the top value on a stack
int ProgramState::top(){
	return stack.top();
}
//check if the stack is empty
bool ProgramState::empty(){
	return stack.empty();
}
//set current line to a new line
void ProgramState::set_curr_line(int lineNum){
	currLine = lineNum;
}
//create a map iterator and print out all values
//in the map
void ProgramState::print_all(){
	std::map<std::string, int>::iterator it;
	for(it = variables.begin(); it != variables.end(); ++it){
		cout << it->first << " " << it->second << endl;
		}
}

std::vector<string> ProgramState::map_to_alpha_vector(){
	string vars;
	string str2;
	string str;
	string_vect.clear();
	std::map<std::string, int>::iterator it;
	for(it = variables.begin(); it != variables.end(); ++it){
		stringstream ss;
		ss << it->second;
		str = ss.str();
		str2 = it->first;
		vars = str2 + " " + str;
		string_vect.push_back(vars);
	}
	return string_vect;
}

std::vector<string> ProgramState::map_to_num_vector(){
	string vars;
	string str2;
	string str;
	string_vect.clear();
	std::map<std::string, int>::iterator it;
	for(it = variables.begin(); it != variables.end(); ++it){
		stringstream ss;
		ss << it->second;
		str = ss.str();
		str2 = it->first;
		vars = str + " " + str2;
		string_vect.push_back(vars);
	}
	return string_vect;
}
