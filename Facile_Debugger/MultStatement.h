#ifndef MULT_STATEMENT_INCLUDED
#define MULT_STATEMENT_INCLUDED

#include "Statement.h"
#include "ProgramState.h"

class MultStatement: public Statement
{
private:
	int first_value;
	int second_value;
	std::string variable_Name2;
	std::string m_variableName;


public:
	MultStatement(std::string, int);	
	MultStatement(std::string, std::string);
	virtual void execute(ProgramState * state, std::ostream &outf);

	
};

#endif