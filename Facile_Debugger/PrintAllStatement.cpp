// PrintStatement.cpp:
#include "PrintAllStatement.h"

PrintAllStatement::PrintAllStatement()
{}


void PrintAllStatement::execute(ProgramState * state, std::ostream &outf)
{
	state->increment();//increment the current line
	state->print_all();//print all variables in the map
	
}


