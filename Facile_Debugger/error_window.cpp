#include "error_window.h"
using namespace std;


ErrorWindow::ErrorWindow()
{	
	//_error = error;
	// Title
	setWindowTitle("ERROR");

	// Overall layout
	overallLayout = new QVBoxLayout();

	buttonsLayout = new QHBoxLayout();
	formLayout = new QHBoxLayout();
	overallLayout->addLayout(formLayout);
	overallLayout->addLayout(buttonsLayout);
	
	//set_text(error);

	hideButton = new QPushButton("hide");
	connect(hideButton, SIGNAL(clicked()), this, SLOT(closeFile()));
	buttonsLayout->addWidget(hideButton);
	setLayout(overallLayout);
	show();

}

ErrorWindow::~ErrorWindow(){}


void ErrorWindow::set_text(std::string _error){
//	ErrorLabel->clear();
	QString qstr = QString::fromStdString(_error);
	ErrorLabel = new QLabel(qstr);
	formLayout->addWidget(ErrorLabel);
	show();
}

void ErrorWindow::clearText(){
	ErrorLabel->clear();
}

void ErrorWindow::closeFile(){
	hide();
}
