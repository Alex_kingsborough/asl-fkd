#include "IfStatement.h"
#include <iostream>
#include <stdlib.h>
using namespace std;
IfStatement::IfStatement(std::string variable, int op, int comp, int newline)
{


	var = variable;
	value2 = comp;
	new_line = newline;
	operator_ = op;

}



void IfStatement::execute(ProgramState * state, ostream &outf)
{
	// TODO
	state->increment();

	int max = state->return_m_numLines();

	value1 = state->return_int(var);

	if(operator_ == -2){
		if (value1 < value2){
			if((new_line < 1) || (new_line > max)){
				cout << "Illegal Jump Instruction" << endl;
				exit (EXIT_FAILURE);
			}
			state->gotoline(new_line);
		}
	}

	if(operator_ == -1){
		if (value1 > value2){
			if((new_line < 1) || (new_line > max)){
				cout << "Illegal Jump Instruction" << endl;
				exit (EXIT_FAILURE);
			}
			state->gotoline(new_line);
		}
	}

	if(operator_ == -22){
		if (value1 <= value2){
			if((new_line < 1) || (new_line > max)){
				cout << "Illegal Jump Instruction" << endl;
				exit (EXIT_FAILURE);
			}
			state->gotoline(new_line);
		}
	}

	if(operator_ == -11){
		if (value1 >= value2){
			if((new_line < 1) || (new_line > max)){
				cout << "Illegal Jump Instruction" << endl;
				exit (EXIT_FAILURE);
			}
			state->gotoline(new_line);
		}
	}

	if(operator_ == -3){
		if (value1 == value2){
			if((new_line < 1) || (new_line > max)){
				cout << "Illegal Jump Instruction" << endl;
				exit (EXIT_FAILURE);
			}
			state->gotoline(new_line);
		}
	}

	if(operator_ == -33){
		if (value1 != value2){
			if((new_line < 1) || (new_line > max)){
				cout << "Illegal Jump Instruction" << endl;
				exit (EXIT_FAILURE);
			}
			state->gotoline(new_line);
		}
	}

}
