#include "debugger_window.h"
#include "ProgramState.h"
#include "Statement.h"
//#include "new_window.h"



debugger_window::debugger_window(std::vector<std::string> _text, ProgramState * first_state, vector<Statement *> program)
{
	text = _text;
	_state = first_state;
	_program = program;

	//first_state->return_steps();
	//&_state = &state;
	// Title
	setWindowTitle("FACILE DEBUGGER");

	overallLayout = new QHBoxLayout();
	
	lineListWidget = new QListWidget();
	int size;
	size = text.size();
	for(int i = 0; i < size; i++){
		QString qstr = QString::fromStdString(text[i]);
		lineListWidget->addItem(qstr);
	}
	//connect
	overallLayout->addWidget(lineListWidget);



	buttonsLayout = new QVBoxLayout();




	overallLayout->addLayout(buttonsLayout);




	breakButton = new QPushButton("Add Break-Point");
	//add signal and slot
	connect(breakButton, SIGNAL(clicked()), this, SLOT(_break()));
	buttonsLayout->addWidget(breakButton);

	continueButton = new QPushButton("Continue");
	//add signal and slot
	connect(continueButton, SIGNAL(clicked()), this, SLOT(_continue()));
	buttonsLayout->addWidget(continueButton);

	stepButton = new QPushButton("Step");
	//add signal and slot
	connect(stepButton, SIGNAL(clicked()), this, SLOT(step()));
	buttonsLayout->addWidget(stepButton);	

	nextButton = new QPushButton("Next");
	//add signal and slot
	connect(nextButton, SIGNAL(clicked()), this, SLOT(next()));
	buttonsLayout->addWidget(nextButton);

	inspectButton = new QPushButton("Inspect");
	//add signal and slot
	connect(inspectButton, SIGNAL(clicked()), this, SLOT(inspect()));
	buttonsLayout->addWidget(inspectButton);

	quitButton = new QPushButton("Quit");
	//add signal and slot
	connect(quitButton, SIGNAL(clicked()), this, SLOT(quit()));
	buttonsLayout->addWidget(quitButton);
	for(int i = 0; i < 1000; i++){
			break_array[i] = 0;
		}



	setLayout(overallLayout);

}

debugger_window::~debugger_window(){}


void debugger_window::_break(){
	int breakpoint;
	breakpoint = lineListWidget->currentRow();
	for(int i = 0; i < 1000; i++){
		if (break_array[i] != 1){
			break_array[i] = 0;
		}
	}
	break_array[breakpoint] = 1;
	for(int j = 0; j < 1000; j++){
		if(break_array[j] == 1){
			lineListWidget->item(j)->setBackground(*(new QBrush(Qt::red)));
		}
	}
}

void debugger_window::_continue(){
	if (_state->getCurrLine() == 0){
		_state->increment();
	}
	while(_state->getCurrLine() != 0){
		_program[_state->getCurrLine()]->execute(_state, cout);
		if (break_array[_state->getCurrLine() - 1] == 1){
			break;
		}
	}
}

void debugger_window::step(){
	if (_state->getCurrLine() == 0){
		_state->increment();
	}
	int s;
	s = text.size();
	for(int i = 0; i < s; i++){
		lineListWidget->item(i)->setBackground(*(new QBrush(Qt::white)));
	}

	if (_state->getCurrLine()-1 >= 0){
		lineListWidget->item(_state->getCurrLine()-1)->setBackground(*(new QBrush(Qt::yellow)));
	}

	_program[_state->getCurrLine()]->execute(_state, cout);


	
	for(int j = 0; j < 1000; j++){
		if(break_array[j] == 1){
			lineListWidget->item(j)->setBackground(*(new QBrush(Qt::red)));
		}
	}
}

void debugger_window::next(){
	if (_state->getCurrLine() == 0){
		_state->increment();
	}
	int a;
	a = text.size();
	for(int i = 0; i < a; i++){
		lineListWidget->item(i)->setBackground(*(new QBrush(Qt::white)));
	}

	if (_state->getCurrLine()-1 >= 0){
		lineListWidget->item(_state->getCurrLine()-1)->setBackground(*(new QBrush(Qt::yellow)));}
	_program[_state->getCurrLine()]->execute(_state, cout);
	while(!_state->empty()){
		_program[_state->getCurrLine()]->execute(_state, cout);
		if (break_array[_state->getCurrLine() - 2] == 1){
			break;
		}
	}
	
	

	for(int j = 0; j < 1000; j++){
		if(break_array[j] == 1){
			lineListWidget->item(j)->setBackground(*(new QBrush(Qt::red)));
		}
	}
}

void debugger_window::inspect(){
	values = new values_window(_state);
	values->show();

}

void debugger_window::quit(){
	this->close();
}
