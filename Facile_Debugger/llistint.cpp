#include "llistint.h"
#include <cstdlib>
#include <stdexcept>
#include <iostream>
using namespace std;


LListInt::LListInt()
{
  head_ = NULL;
  tail_ = NULL;
  size_ = 0;
}


LListInt::LListInt(const LListInt& other){
  head_= NULL;
  tail_ = NULL;
  size_ = 0;
  for (int i = 0; i < other.size() ; i++){
    insert(i, other.get(i));
  }
}

LListInt& LListInt:: operator=(const LListInt& other){
  this->clear();

  for(int j = 0; j < other.size_; j++){
    insert(j, other.get(j));
  }
  return *this;
}


LListInt LListInt::operator+(const LListInt& other) const{
  LListInt * list = new LListInt();
  *list = *this;

for (int i = 0; i < other.size_; i++){
  list->insert((list->size_), other.get(i));
  //cout << "other.get(" << i << ")" << other.get(i) << endl;
}

  return *list;
}

int const& LListInt::operator[](int position) const{
  return get(position);
}









LListInt::~LListInt()
{
  clear();
}

bool LListInt::empty() const
{
  return size_ == 0;
}

int LListInt::size() const
{
  return size_;
}

/**
 * Complete the following function
 */
void LListInt::insert(int loc, const int& val)
{
  if(loc >= 0 && loc <= size_){
   
    Item* n = new Item;
    n->next = NULL;
    n->val = val;
   
   if(head_ != NULL){
   
   if(loc == 0){




      n->next=head_;
      n->prev=head_->prev;
      head_->prev = n;
      head_=n;
    }
   else{
   
   
   
    //Item* n = new Item;
    //n->next = NULL;
    //n->val = val;

    if (head_ != NULL){


      Item* temp;
      if(getNodeAt(loc) != 0){//check if inserting at end of list
      temp = getNodeAt(loc);// if not inserting at end of list
      // doubly link new node into list
        n->next = temp;
        n->prev = temp->prev;
        temp->prev = n;
        if(n->prev != 0){
        n->prev->next = n;
        }
      }//end if
      else{
        temp = getNodeAt(size_-1);//if inserting at end of list get node 
        // before end to link to new node
        temp->next = n;
        n->prev = temp;
        tail_ = n;//change tail to new tail
      } //end else
    }//end if
    }
    }
    else{
      head_ = n;
    }//end else
      size_ = size_ + 1;//increase size by one
  }//end if

  
  else{
    cout << "location cannot be added to" << endl;//if the location was 
    //invalid, tell user that it could not be added
  }//end else
}

/**
 * Complete the following function
 */
void LListInt::remove(int loc)
{

  Item* delPtr = NULL;//make a pointer to be deleted
  Item* temp;
  temp = getNodeAt(loc);//make temp nodes to patch together list without deleted node
  Item* curr;
  curr = getNodeAt(loc);

  if (loc == 0){
    head_=head_->next;
  }

  if(curr == 0){//cout if the item couldnt be deleted for some reason
    //specifically if the location did not exist
    cout << "location could not be deleted" << endl;
  }
  else{
    delPtr = curr;
    if (loc != (size_ - 1)){//check and see if item is the end of the list
    curr = curr->next;//move one temp node to the next node
    }
    if (loc != 0){//check and see if the item is the first element
    temp = temp->prev;//move one temp node to the prev node
    }
    curr->prev = temp;//doubly link the the nodes on either side of the deleted node
    temp->next = curr;
    delete delPtr;//delete the node to be deleted
    size_ = size_ - 1;//decrease the size
    if (size_ == 0){//if the size is zero set head and tail to NULL
      head_ = NULL;
      tail_ = NULL;
    }
  }
}

void LListInt::set(int loc, const int& val)
{
  Item *temp = getNodeAt(loc);
  temp->val = val;
}

int& LListInt::get(int loc)
{
  if(loc < 0 || loc >= size_){
    throw std::invalid_argument("bad location");
  }
  Item *temp = getNodeAt(loc);
  return temp->val;
}

int const & LListInt::get(int loc) const
{
  if(loc < 0 || loc >= size_){
    throw std::invalid_argument("bad location");
  }
  Item *temp = getNodeAt(loc);
  return temp->val;
}

void LListInt::clear()
{
  while(head_ != NULL){
    Item *temp = head_->next;
    delete head_;
    head_ = temp;
  }
  tail_ = NULL;
  size_ = 0;
}


/**
 * Complete the following function
 */
LListInt::Item* LListInt::getNodeAt(int loc) const
{
  Item* curr;
  if (loc < size_){//make sure getting node at valid location
  
  curr = head_;
  for (int i = 0; i < loc; i++){
    if (curr->next != NULL){
      curr = curr->next;//find the correct node by incrementing
      //through the nodes "loc" number of times
    }
  }
  return curr;//return a pointer to the node
}
else {
  curr = NULL;
  return curr; //if the loc is not valid, return NULL pointer
}
}
