#ifndef IF_STATEMENT_INCLUDED
#define IF_STATEMENT_INCLUDED

#include "Statement.h"
#include "ProgramState.h"

class IfStatement: public Statement
{
private:
	std::string var;
	int operator_;
	int value1;
	int value2;
	int new_line;	

public:
	IfStatement(std::string, int, int, int);
	virtual void execute(ProgramState * state, std::ostream &outf);

	
};

#endif