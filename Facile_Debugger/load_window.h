#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QListWidget>
#include <string>
#include <vector>
#include <QApplication>
#include <QShortcut>


#include "Statement.h"
#include "LetStatement.h"
#include "EndStatement.h"
#include "PrintStatement.h"
#include "AddStatement.h"
#include "SubStatement.h"
#include "MultStatement.h"
#include "DivStatement.h"
#include "GotoStatement.h"
#include "IfStatement.h"
#include "GoSubStatement.h"
#include "ReturnStatement.h"
#include "PrintAllStatement.h"
#include <vector>
#include <string>
#include <sstream> 
#include <fstream>
#include <cstdlib>
#include <iostream>

#include <istream>
#include "Statement.h"
#include "ProgramState.h"
//#include <load_window.cpp>
#include "debugger_window.h"
#include "error_window.h"


class LoadWindow : public QWidget
{
	Q_OBJECT
public:
	LoadWindow();
	~LoadWindow();

private slots:
	void loadFile();
	void quit();



private:

	QVBoxLayout* overallLayout;
	QHBoxLayout* buttonsLayout;
	QHBoxLayout* formLayout;
	QLabel* FilenameLabel;
	QLineEdit* FilenameInput;
	QPushButton* LoadFileButton;
	QPushButton* quitButton;
	debugger_window* debugger;
	std::vector<std::string> lines;
	ErrorWindow* error;

	// parseProgram() takes a filename as a parameter, opens and reads the
// contents of the file, and returns an vector of pointers to Statement.
void parseProgram(istream& inf, vector<Statement *> & program);

// parseLine() takes a line from the input file and returns a Statement
// pointer of the appropriate type.  This will be a handy method to call
// within your parseProgram() method.
Statement * parseLine(string line);

// interpretProgram() reads a program from the given input stream
// and interprets it, writing any output to the given output stream.
// Note:  you are required to implement this function!
void interpretProgram(istream& inf, ostream& outf);


};