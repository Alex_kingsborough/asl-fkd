#ifndef ERROR_WINDOW
#define ERROR_WINDOW

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QListWidget>
#include <QString>
#include <string>
#include <vector>
#include <fstream>
#include <QApplication>
#include <QCloseEvent>
#include <QListWidget>
#include <iostream>

#include <QtGui>

#include <sstream> 
#include <cstdlib>

class ErrorWindow : public QWidget
{
	Q_OBJECT
public:
	ErrorWindow();
	~ErrorWindow();
	void set_text(std::string);
	void clearText();
private slots:
	void closeFile();

private:

	QVBoxLayout* overallLayout;
	QHBoxLayout* buttonsLayout;
	QHBoxLayout* formLayout;
	QPushButton* hideButton;
	QLabel* ErrorLabel;
};

#endif